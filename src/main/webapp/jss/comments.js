var comment = angular.module('Comment', []);

comment.controller('Com', ['$scope','$http',function($scope, $http){    //linking our controller to our namespace ie institution

	$scope.commentForm = true;
	$scope.commentTable = false;

	$scope.createComment = function(){
		showHideFormTable(false, true);
	}

	$scope.showComment = function(){
		showHideFormTable(true, false);
	}

	function showHideFormTable(form, table){
		$scope.commentForm = form;
		$scope.commentTable = table;
	}

	$scope.comment = {}; 
	$scope.comments = {};

	$scope.saveComment = function(){

		$http({
			url: '../api/comment',
			method: 'POST',
			data: $.param($scope.comment),
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).success(function(data){
			getComments();
			$scope.comment = {};         

		});

	}

	function getComments(){
		$http.get('../api/comment').
		success(function(data){
			$scope.comments = data.comments;

		});

	}

	getComments();

	$scope.loadEdit = function(idx){
		$http.get('../api/comment/edit/' + $scope.comments[idx].id).
		success(function(data){           // $scope.ester
			$scope.comment = data;
			showHideFormTable(false, true);

		});
	}

	$scope.delete = function(idx){
		$http({
			url: '../api/comment/' + $scope.comments[idx].id,
			method: 'DELETE'
		}).success(function(data){
			getComments();

		});
	}

	$scope.search = function(){
		$http.get('../api/comment/' + $scope.name_search).
		success(function(data){           
			$scope.comments = data.comments;

		});
	}

	$scope.reset = function(){
		getComments();
		$scope.name_search = '';
	}

}]);
