var designer = angular.module('Designer', []);

designer.controller('Design',['$scope','$http', function($scope, $http){    //linking our controller to our namespace ie institution

	$scope.designerForm = true;
	$scope.designerTable = false;

	$scope.createDesigner = function(){
		showHideFormTable(false, true);
	}

	$scope.showDesigner = function(){
		showHideFormTable(true, false);
	}

	function showHideFormTable(form, table){
		$scope.designerForm = form;
		$scope.designerTable = table;
	}

	$scope.designer = {}; 
	$scope.designers = {};

	$scope.saveDesigner = function(){

		$http({
			url: '../api/designer',
			method: 'POST',
			data: $.param($scope.designer),
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).success(function(data){
			getDesigners();
			$scope.designer = {};         

		});

	}

	function getDesigners(){
		$http.get('../api/designer').
		success(function(data){
			$scope.designers = data.designers;

		});

	}

	getDesigners();

	$scope.loadEdit = function(idx){
		$http.get('../api/designer/edit/' + $scope.designers[idx].id).
		success(function(data){           // $scope.ester
			$scope.designer = data;
			showHideFormTable(false, true);

		});
	}

	$scope.delete = function(idx){
		$http({
			url: '../api/designer/' + $scope.designers[idx].id,
			method: 'DELETE'
		}).success(function(data){
			getDesigners();

		});
	}

	$scope.search = function(){
		$http.get('../api/designer/' + $scope.name_search).
		success(function(data){           
			$scope.designers = data.designers;

		});
	}

	$scope.reset = function(){
		getDesigners();
		$scope.name_search = '';
	}

}]);
