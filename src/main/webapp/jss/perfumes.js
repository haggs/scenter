var perfume = angular.module('Perfume', []);

perfume.controller('Per', ['$scope','$http', function($scope, $http){    //linking our controller to our namespace ie institution

	$scope.perfumeForm = true;
	$scope.perfumeTable = false;

	$scope.createPerfume = function(){
		showHideFormTable(false, true);
	}

	$scope.showPerfume = function(){
		showHideFormTable(true, false);
	}

	function showHideFormTable(form, table){
		$scope.perfumeForm = form;
		$scope.perfumeTable = table;
	}

	$scope.perfume = {}; 
	$scope.perfumes = {};

	$scope.savePerfume = function(){

		$http({
			url: '../api/perfume',
			method: 'POST',
			data: $.param($scope.perfume),
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).success(function(data){
			getPerfumes();
			$scope.perfume = {};         

		});

	}

	function getPerfumes(){
		$http.get('../api/perfume').
		success(function(data){
			$scope.perfumes = data.perfumes;

		});

	}

	getPerfumes();

	$scope.loadEdit = function(idx){
		$http.get('../api/perfume/edit/' + $scope.perfumes[idx].id).
		success(function(data){           // $scope.ester
			$scope.perfume = data;
			showHideFormTable(false, true);

		});
	}

	$scope.delete = function(idx){
		$http({
			url: '../api/perfume/' + $scope.perfumes[idx].id,
			method: 'DELETE'
		}).success(function(data){
			getPerfumes();

		});
	}

	$scope.search = function(){
		$http.get('../api/perfume/' + $scope.name_search).
		success(function(data){           
			$scope.perfumes = data.perfumes;

		});
	}

	$scope.reset = function(){
		getPerfumes();
		$scope.name_search = '';
	}

}]);
