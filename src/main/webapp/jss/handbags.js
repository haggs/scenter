var handBag = angular.module('HandBags', []);

handBag.controller('Handb', ['$scope','$http', function($scope, $http){    //linking our controller to our namespace ie institution

	$scope.handBagForm = true;
	$scope.handBagTable = false;

	$scope.createHandBag = function(){
		showHideFormTable(false, true);
	}

	$scope.showHandBag = function(){
		showHideFormTable(true, false);
	}

	function showHideFormTable(form, table){
		$scope.handBagForm = form;
		$scope.handBagTable = table;
	}

	$scope.handBag = {}; 
	$scope.handBags = {};

	$scope.saveHandBag = function(){

		$http({
			url: '../api/handBag',
			method: 'POST',
			data: $.param($scope.handBag),
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).success(function(data){
			getHandBags();
			$scope.handBag = {};         

		});

	}

	function getHandBags(){
		$http.get('../api/handBag').
		success(function(data){
			$scope.handBags = data.handBags;

		});

	}

	getHandBags();

	$scope.loadEdit = function(idx){
		$http.get('../api/handBag/edit/' + $scope.handBags[idx].id).
		success(function(data){           // $scope.ester
			$scope.handBag = data;
			showHideFormTable(false, true);

		});
	}

	$scope.delete = function(idx){
		$http({
			url: '../api/handBag/' + $scope.handBags[idx].id,
			method: 'DELETE'
		}).success(function(data){
			getHandBags();

		});
	}

	$scope.search = function(){
		$http.get('../api/handBag/' + $scope.name_search).
		success(function(data){           
			$scope.handBags = data.handBags;

		});
	}

	$scope.reset = function(){
		getHandBags();
		$scope.name_search = '';
	}

}]);
