package com.scenter.serverapi.comments;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.jboss.resteasy.annotations.Form;
import com.scenter.model.comments.Comment;
import com.scenter.model.comments.CommentsWrapper;

@Path("/comment")
public class CommentApi {
	
	@PersistenceUnit
	private EntityManagerFactory emf;
	
	
	// Post comments to Database
	@POST
	public String create (@Form Comment com){
	
	EntityManager em = emf.createEntityManager();
	
	try{
	em.getTransaction().begin();
	em.merge(com);
	em.getTransaction().commit();
	
	return "{\"success\": true, \"msg\": \"Saved Successfully \"}";
	}catch(Exception e){
		e.printStackTrace();
		em.getTransaction().rollback();
			
	}

	return "{\"success\": false,  \"msg\": \"Error Occurred!\"}";
	}
	
	//Get from Database
	@SuppressWarnings("unchecked")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	
	public Response list(){
		EntityManager em = emf.createEntityManager();
		
		List <Comment>comments = em.createNamedQuery("Comment.findAll").getResultList();
		CommentsWrapper wrapper = new CommentsWrapper ();
		wrapper.setComments(comments);
		return Response.status(200).entity(wrapper).build();  // supply an entity of the response with the entity() & ok notific
			
	}

}
