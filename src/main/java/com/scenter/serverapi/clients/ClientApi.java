package com.scenter.serverapi.clients;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.jboss.resteasy.annotations.Form;
import org.jboss.resteasy.annotations.providers.multipart.MultipartForm;

import com.scenter.model.clients.Client;
import com.scenter.model.clients.ClientsWrapper;

@Path("/client")
public class ClientApi {

	@PersistenceUnit
	private EntityManagerFactory emf;

	// Posts to the database

	@POST
	public String create(@Form Client cli) {

		EntityManager em = emf.createEntityManager();

		try {
			em.getTransaction().begin();
			em.merge(cli);
			em.getTransaction().commit();

			return "{\"success\": true, \"msg\": \"Saved Successfully \"}";
		} catch (Exception e) {
			e.printStackTrace();
			em.getTransaction().rollback();

		}

		return "{\"success\": false,  \"msg\": \"Error Occurred!\"}";
	}

	// For uploading photos

	@POST
	@Path("/upload")
	@Consumes("multipart/form-data")
	public Response uploadFile(@MultipartForm Client form) {

		String fileName = form.getFileName() == null ? "Unknown" : form
				.getFileName();

		String completeFilePath = "c:/temp/" + fileName;
		try {
			// Save the file
			File file = new File(completeFilePath);

			if (!file.exists()) {
				file.createNewFile();
			}

			FileOutputStream fos = new FileOutputStream(file);

			fos.write(form.getFileData());
			fos.flush();
			fos.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		// Build a response to return
		return Response
				.status(200)
				.entity("uploadFile is called, Uploaded file name : "
						+ fileName).build();
	}

	// Retrieves from the database
	@SuppressWarnings("unchecked")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response list() {
		EntityManager em = emf.createEntityManager();

		List<Client> clients = em.createNamedQuery("Client.findAll")
				.getResultList();
		ClientsWrapper wrapper = new ClientsWrapper();
		wrapper.setClients(clients);
		return Response.status(200).entity(wrapper).build();
	}

	// For searching by name from the database
	@SuppressWarnings("unchecked")
	@GET
	@Path("/{name}")
	// {}
	@Produces(MediaType.APPLICATION_JSON)
	public Response search(@PathParam("name") String name) {
		EntityManager em = emf.createEntityManager();

		List<Client> clients = em.createNamedQuery("Client.findByname")
				.setParameter("cliName", name).getResultList();

		ClientsWrapper wrapper = new ClientsWrapper();
		wrapper.setClients(clients);

		return Response.status(200).entity(wrapper).build();
	}

	// For editing individual instance
	@GET
	@Path("/edit/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response loadEdit(@PathParam("id") Long id) {
		EntityManager em = emf.createEntityManager();

		Client cli = em.find(Client.class, id);

		return Response.status(200).entity(cli).build();
	}

	// For deleting an instance

	@DELETE
	@Path("/{id}")
	public String delete(@PathParam("id") Long id) {

		EntityManager em = emf.createEntityManager();

		try {
			Client clients = em.find(Client.class, id);
			em.getTransaction().begin();
			em.remove(clients);
			em.getTransaction().commit();

			return "{\"success\": true, \"msg\": \"Deleted Successfully \"}";
		} catch (Exception ex) {
			ex.printStackTrace();
			em.getTransaction().rollback();

		}

		return "{\"success\": false,  \"msg\": \"Error Occurred!\"}";
	}

	// For uploading photos

	/*
	 * @POST
	 * 
	 * @Path("/upload")
	 * 
	 * @Consumes("multipart/form-data") public Response
	 * uploadFile(@MultipartForm FileUpload form) {
	 * 
	 * String fileName = form.getFileName() == null ? "Unknown" :
	 * form.getFileName() ;
	 * 
	 * String completeFilePath = "c:/temp/" + fileName; try { //Save the file
	 * File file = new File(completeFilePath);
	 * 
	 * if (!file.exists()) { file.createNewFile(); }
	 * 
	 * FileOutputStream fos = new FileOutputStream(file);
	 * 
	 * fos.write(form.getFileData()); fos.flush(); fos.close(); } catch
	 * (IOException e) { e.printStackTrace(); } //Build a response to return
	 * return Response.status(200)
	 * .entity("uploadFile is called, Uploaded file name : " +
	 * fileName).build(); }
	 */

}
