package com.scenter.serverapi.designers;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.jboss.resteasy.annotations.Form;

import com.scenter.model.designers.Designer;
import com.scenter.model.designers.DesignersWrapper;

@Path("/designer")
public class DesignerApi {

	@PersistenceUnit
	private EntityManagerFactory emf;

	@POST
	public String create(@Form Designer des) {

		EntityManager em = emf.createEntityManager();

		try {
			em.getTransaction().begin();
			em.merge(des);
			em.getTransaction().commit();

			return "{\"success\": true, \"msg\": \"Saved Successfully \"}";
		} catch (Exception e) {
			e.printStackTrace();
			em.getTransaction().rollback();

		}

		return "{\"success\": false,  \"msg\": \"Error Occurred!\"}";
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	//@Produces(MediaType.APPLICATION_JSON)
	public Response list() {
		EntityManager em = emf.createEntityManager();

		@SuppressWarnings("unchecked")
		List<Designer> designers = em.createNamedQuery("Designer.findAll")
				.getResultList();

		DesignersWrapper wrapper = new DesignersWrapper();
		wrapper.setDesigners(designers);

		return Response.status(200).entity(wrapper).build();
	}

	@SuppressWarnings("unchecked")
	@GET
	@Path("/{name}") //{}
	@Produces(MediaType.APPLICATION_JSON)
	public Response search(@PathParam("name") String name){
		EntityManager em = emf.createEntityManager();
		
		List<Designer> designers = em.createNamedQuery("Designer.findByname")
				.setParameter("desName", name).getResultList();
		
		DesignersWrapper wrapper = new DesignersWrapper();
		wrapper.setDesigners(designers);
		
		return Response.status(200).entity(wrapper).build();
	}
	
	@GET
	@Path("/edit/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response loadEdit(@PathParam("id") Long id){
		EntityManager em = emf.createEntityManager();
		
		Designer des = em.find(Designer.class, id);
		
		return Response.status(200).entity(des).build();
	}
	
	
	@DELETE
	@Path("/{id}")
	public String delete(@PathParam("id") Long id){
		
		EntityManager em = emf.createEntityManager();
		
		try{
			Designer designers = em.find(Designer.class, id);
			em.getTransaction().begin();
			em.remove(designers);
			em.getTransaction().commit();
			
			return "{\"success\": true, \"msg\": \"Deleted Successfully \"}";
		}catch(Exception ex){
			ex.printStackTrace();
			em.getTransaction().rollback();
			
		}
		
		return "{\"success\": false,  \"msg\": \"Error Occurred!\"}";
	}
	
	
}
