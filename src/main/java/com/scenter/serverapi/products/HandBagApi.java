package com.scenter.serverapi.products;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.jboss.resteasy.annotations.Form;

import com.scenter.model.products.HandBag;
import com.scenter.model.products.HandBagsWrapper;

@Path ("/handBag")
public class HandBagApi {
	
	@PersistenceUnit
	
	private EntityManagerFactory emf;
	
	@POST
	public String create(@Form HandBag hbag){
		EntityManager em = emf.createEntityManager();
		try{
			em.getTransaction().begin();
			em.merge(hbag);
			em.getTransaction().commit();
			
			return "{\"success\": true, \"msg\": \"Saved Successfully \"}";
			
		}catch(Exception e){
			e.printStackTrace();
			em.getTransaction().rollback();
			
		}
		return "{\"success\": false,  \"msg\": \"Error Occurred!\"}";
	}
	
	
	@SuppressWarnings("unchecked")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	
	public Response list(){
		EntityManager em = emf.createEntityManager();
		
		List <HandBag>handbags = em.createNamedQuery("HandBag.findAll").getResultList();
		HandBagsWrapper wrapper = new HandBagsWrapper ();
		wrapper.setHandBags(handbags);
		return Response.status(200).entity(wrapper).build();  // supply an entity of the response with the entity() & ok notific
			
	}
	

	@SuppressWarnings("unchecked")
	@GET
	@Path("/{name}") //{}
	@Produces(MediaType.APPLICATION_JSON)
	public Response search(@PathParam("name") String name){
		EntityManager em = emf.createEntityManager();
		
		List<HandBag> handBags = em.createNamedQuery("HandBag.findByName")
				.setParameter("hbName", name).getResultList();
		
		HandBagsWrapper wrapper = new HandBagsWrapper();
		wrapper.setHandBags(handBags);
		
		return Response.status(200).entity(wrapper).build();
	}
	
	@GET
	@Path("/edit/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response loadEdit(@PathParam("id") Long id){
		EntityManager em = emf.createEntityManager();
		
		HandBag hand = em.find(HandBag.class, id);
		
		return Response.status(200).entity(hand).build();
	}
	
	
	@DELETE
	@Path("/{id}")
	public String delete(@PathParam("id") Long id){
		
		EntityManager em = emf.createEntityManager();
		
		try{
			HandBag hands = em.find(HandBag.class, id);
			em.getTransaction().begin();
			em.remove(hands);
			em.getTransaction().commit();
			
			return "{\"success\": true, \"msg\": \"Deleted Successfully \"}";
		}catch(Exception ex){
			ex.printStackTrace();
			em.getTransaction().rollback();
			
		}
		
		return "{\"success\": false,  \"msg\": \"Error Occurred!\"}";
	}
	
	

}
