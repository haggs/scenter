package com.scenter.serverapi.products;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.jboss.resteasy.annotations.Form;
import com.scenter.model.products.Perfume;
import com.scenter.model.products.PerfumesWrapper;

@Path("/perfume")
public class PerfumeApi {

	@PersistenceUnit
	private EntityManagerFactory emf;

	@POST
	public String create(@Form Perfume per) {
		EntityManager em = emf.createEntityManager();
		try {
			em.getTransaction().begin();
			em.merge(per);
			em.getTransaction().commit();

			return "{\"success\": true, \"msg\": \"Saved Successfully \"}";

		} catch (Exception e) {
			e.printStackTrace();
			em.getTransaction().rollback();

		}
		return "{\"success\": false,  \"msg\": \"Error Occurred!\"}";
	}

	@SuppressWarnings("unchecked")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response list() {
		EntityManager em = emf.createEntityManager();

		List<Perfume> perfumes = em.createNamedQuery("Perfume.findAll")
				.getResultList();
		PerfumesWrapper wrapper = new PerfumesWrapper();
		wrapper.setPerfumes(perfumes);
		return Response.status(200).entity(wrapper).build(); 
	}

}
