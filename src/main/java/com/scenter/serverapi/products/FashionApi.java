package com.scenter.serverapi.products;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.jboss.resteasy.annotations.Form;

import com.scenter.model.products.Fashion;
import com.scenter.model.products.FashionsWrapper;

@Path("/fashion")
public class FashionApi {
	
	@PersistenceUnit
	private EntityManagerFactory emf;
	
	@POST
	public String create (@Form Fashion fas){
	
	EntityManager em = emf.createEntityManager();
	
	try{
	em.getTransaction().begin();
	em.merge(fas);
	em.getTransaction().commit();
	
	return "{\"success\": true, \"msg\": \"Saved Successfully \"}";
	}catch(Exception e){
		e.printStackTrace();
		em.getTransaction().rollback();
			
	}

	return "{\"success\": false,  \"msg\": \"Error Occurred!\"}";
	}
	
	@SuppressWarnings("unchecked")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	
	public Response list(){
		EntityManager em = emf.createEntityManager();
		
		List <Fashion>fashions = em.createNamedQuery("Fashion.findAll").getResultList();
		FashionsWrapper wrapper = new FashionsWrapper ();
		wrapper.setFashions(fashions);
		return Response.status(200).entity(wrapper).build();  // supply an entity of the response with the entity() & ok notific
			
	}

	@SuppressWarnings("unchecked")
	@GET
	@Path("/{name}") //{}
	@Produces(MediaType.APPLICATION_JSON)
	public Response search(@PathParam("name") String name){
		EntityManager em = emf.createEntityManager();
		
		List<Fashion> fashions = em.createNamedQuery("Fashion.findByName")
				.setParameter("fasName", name).getResultList();
		
		FashionsWrapper wrapper = new FashionsWrapper();
		wrapper.setFashions(fashions);
		
		return Response.status(200).entity(wrapper).build();
	}
	
	@GET
	@Path("/edit/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response loadEdit(@PathParam("id") Long id){
		EntityManager em = emf.createEntityManager();
		
		Fashion fas = em.find(Fashion.class, id);
		
		return Response.status(200).entity(fas).build();
	}
	
	
	@DELETE
	@Path("/{id}")
	public String delete(@PathParam("id") Long id){
		
		EntityManager em = emf.createEntityManager();
		
		try{
			Fashion fashions = em.find(Fashion.class, id);
			em.getTransaction().begin();
			em.remove(fashions);
			em.getTransaction().commit();
			
			return "{\"success\": true, \"msg\": \"Deleted Successfully \"}";
		}catch(Exception ex){
			ex.printStackTrace();
			em.getTransaction().rollback();
			
		}
		
		return "{\"success\": false,  \"msg\": \"Error Occurred!\"}";
	}
	
	
}
