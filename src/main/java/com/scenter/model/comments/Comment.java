package com.scenter.model.comments;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.ws.rs.FormParam;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.scenter.model.common.BaseEntity;


@XmlRootElement(name = "Comment")
@NamedQueries({
		@NamedQuery(
				name = "Comment.findAll",
				query = "FROM Comment t" 
																	
		),
		@NamedQuery(
				name = "Comment.countAll", 
				query = "SELECT COUNT(*) FROM Comment t"
		),
		/*@NamedQuery(
				name = "Comment.findByname",
				query = "FROM Comment t WHERE t.name=:comName")*/
				
		})

@Entity
@Table(name="com_comments")
public class Comment extends BaseEntity {
	private static final long serialVersionUID = 1L;
		
	@FormParam("comment")
	@Column
	private String comment;

    @XmlElement
	public String getComment() {
		return comment;
	}


	public void setComment(String comment) {
		this.comment = comment;
	}
	
	

}
