package com.scenter.model.products;

import java.util.ArrayList;
import java.util.List;

public class PerfumesWrapper {
	private List <Perfume> perfumes = new ArrayList <Perfume>();

	public List<Perfume> getPerfumes() {
		return perfumes;
	}

	public void setPerfumes(List<Perfume> perfumes) {
		this.perfumes = perfumes;
	}
	


}
