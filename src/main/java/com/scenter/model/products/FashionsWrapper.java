package com.scenter.model.products;

import java.util.ArrayList;
import java.util.List;


public class FashionsWrapper {
	private List <Fashion> fashions = new ArrayList <Fashion>();

	public List<Fashion> getFashions() {
		return fashions;
	}

	public void setFashions(List<Fashion> fashions) {
		this.fashions = fashions;
	}
	

}
