package com.scenter.model.products;

import java.util.ArrayList;
import java.util.List;

public class HandBagsWrapper {
	private List <HandBag> handBags = new ArrayList <HandBag>();

	public List<HandBag> getHandBags() {
		return handBags;
	}

	public void setHandBags(List<HandBag> handBags) {
		this.handBags = handBags;
	}
	


}
