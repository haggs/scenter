package com.scenter.model.products;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.ws.rs.FormParam;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.scenter.model.common.BaseEntity;

@XmlRootElement(name = "Fashion")
@NamedQueries({
		@NamedQuery(
				name = "Fashion.findAll",
				query = "FROM Fashion f" 
																	
		),
		@NamedQuery(
				name = "Fashion.countAll", 
				query = "SELECT COUNT(*) FROM Fashion f"
		),
		@NamedQuery(
				name = "Fashion.findByName",
				query = "FROM Fashion f WHERE f.name=:fasName")
				
		})

@Entity
@Table (name = "pro_fashions")
public class Fashion extends BaseEntity {
	private static final long serialVersionUID = 1L;
	
	@FormParam("name")
	@Column
	private String name; 		//suppliers name
	
	//private File picture;
	
	@FormParam("desc")
	@Column(name = "descr")
	private String description;
	
/*
	@Form(prefix="address")List<Address> addreses;
	@Embedded
	private Address address;
*/
	
	@XmlElement
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@XmlElement
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	/*@XmlElement
	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}*/

}
