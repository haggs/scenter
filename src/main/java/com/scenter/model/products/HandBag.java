package com.scenter.model.products;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.ws.rs.FormParam;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.scenter.model.common.BaseEntity;

@XmlRootElement(name = "HandBag")
@NamedQueries({
		@NamedQuery(
				name = "HandBag.findAll",
				query = "FROM HandBag h" 
																	
		),
		@NamedQuery(
				name = "HandBag.countAll", 
				query = "SELECT COUNT(*) FROM HandBag h"
		),
		@NamedQuery(
				name = "HandBag.findByname",
				query = "FROM HandBag h WHERE h.name=:hbName")
				
		})


@Entity
@Table (name = "pro_handBags")
public class HandBag extends BaseEntity {
	private static final long serialVersionUID = 1L;
	
	@FormParam("name")
	@Column
	private String name; 		//suppliers name
	
	//private File picture;
		
	@FormParam("desc")
	@Column
	private String description;
	
	/*@Form(prefix = "address")
	@Embedded
	private Address address;
*/
	@XmlElement
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@XmlElement
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	/*@XmlElement
	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}*/
	
	

}
