package com.scenter.model.common;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.ws.rs.FormParam;


@Embeddable
public class Address {
	@FormParam("postalAddress")
	@Column(name = "postal_address")
	private String postalAddress;
	
	@FormParam("postalCode")
	@Column(name = "postal_code")
	private String postalCode;
	
	@FormParam("phoneNo")
	@Column(name = "phone_no")
	private String phoneNo;
	
	@FormParam("email")
	@Column
	private String email;
	
	@FormParam("website")
	@Column
	private String website;
	
	@FormParam("town")
	@Column
	private String town;
	
	@FormParam("county")
	@Column
	private String county;

	public String getPostalAddress() {
		return postalAddress;
	}

	public void setPostalAddress(String postalAddress) {
		this.postalAddress = postalAddress;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getPhoneNo() {
		return phoneNo;
	}

	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public String getTown() {
		return town;
	}

	public void setTown(String town) {
		this.town = town;
	}

	public String getCounty() {
		return county;
	}

	public void setCounty(String country) {
		this.county = country;
	}
}
