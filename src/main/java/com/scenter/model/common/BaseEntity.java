package com.scenter.model.common;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.ws.rs.FormParam;
import javax.xml.bind.annotation.XmlElement;

@MappedSuperclass
public class BaseEntity implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@FormParam("id")
	@Id@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(name = "time_created")
	@Temporal(TemporalType.TIMESTAMP)
	private Date timeCreated;
	
	@Column(name = "time_modified")
	@Temporal(TemporalType.TIMESTAMP)
	private Date timeModified;

	@XmlElement
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@XmlElement(name = "time_created")
	public Date getTimeCreated() {
		return timeCreated;
	}

	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}

	@XmlElement(name = "time_modified")
	public Date getTimeModified() {
		return timeModified;
	}

	public void setTimeModified(Date timeModified) {
		this.timeModified = timeModified;
	}

}
