package com.scenter.model.clients;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.ws.rs.FormParam;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.jboss.resteasy.annotations.providers.multipart.PartType;

import com.scenter.model.common.BaseEntity;


@XmlRootElement(name = "Client")
@NamedQueries({
		@NamedQuery(
				name = "Client.findAll",
				query = "FROM Client c" 
																	
		),
		@NamedQuery(
				name = "Client.countAll", 
				query = "SELECT COUNT(*) FROM Client c"
		),
		@NamedQuery(
				name = "Client.findByname",
				query = "FROM Client c WHERE c.name=:cliName")
				
		})

@Entity
@Table(name = "cli_clients")
public class Client extends BaseEntity {

	private static final long serialVersionUID = 1L;

	@FormParam("name")
	@Column
	private String name;
	
	/*@Form(prefix = "address")
	@Embedded
	private Address address;
	*/
	
	//For file upload
		
	@FormParam("fileName")
	@Column
	private String fileName;
	
	
	@FormParam("file")
	@Column (name = "file")
	private byte[] fileData;
	
	@FormParam("purpose")
	@Column(name = "purpose")
	private String  purpose ;
	
	
	@FormParam("contact")
	@Column(name = "contacts")
	private String contact ;
    
	//Getters and setters
	
	@XmlElement
    public byte[] getFileData() {
		return fileData;
	}

    @FormParam("fileName")
	public void setFileData(byte[] fileData) {
		this.fileData = fileData;
	}

	@XmlElement
	public String getFileName() {
		return fileName;
	}

	
    @PartType("application/octet-stream")
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

			
	@XmlElement
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
/*
	@XmlElement
	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}*/

	@XmlElement
	public void setPurpose(String purpose) {
		this.purpose = purpose;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}
	@XmlElement
	public String getContact() {
		return contact;
	}

	public String getPurpose() {
		return purpose;
	}

}
