package com.scenter.model.clients;

import java.util.ArrayList;
import java.util.List;


public class ClientsWrapper {
	
	private List <Client>clients = new ArrayList <Client>();

	public List<Client> getClients() {
		return clients;
	}

	public void setClients(List<Client> clients) {
		this.clients = clients;
	}
	

}
