package com.scenter.model.designers;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.ws.rs.FormParam;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.scenter.model.common.BaseEntity;


@XmlRootElement(name = "Designers")
@NamedQueries({
		@NamedQuery(
				name = "Designer.findAll",
				query = "FROM Designer k" 
																	
		),
		@NamedQuery(
				name = "Designer.countAll", 
				query = "SELECT COUNT(*) FROM Designer k"
		),
		/*@NamedQuery(
				name = "Designer.findByname",
				query = "FROM Designer k WHERE k.name=:desName")
				*/
		})
@Entity
@Table(name = "des_designers")
public class Designer extends BaseEntity  {

	 
	private static final long serialVersionUID = 1L;


	@FormParam("name")
	@Column
	private String companyName;
/*
	@Form(prefix = "address")
	@Embedded
	private Address address;
	
	
	@XmlElement
	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}
*/
	@FormParam("desc")
	@Column(name = "descr")
	private String description;

	@XmlElement
	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	
	@XmlElement
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	
}
