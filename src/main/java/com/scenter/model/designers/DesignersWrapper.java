package com.scenter.model.designers;

import java.util.ArrayList;
import java.util.List;



public class DesignersWrapper {
	
	private List < Designer>designers = new ArrayList < Designer>();

	public List<Designer> getDesigners() {
		return designers;
	}

	public void setDesigners(List<Designer> designers) {
		this.designers = designers;
	}
	

}
