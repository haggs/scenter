var client = angular.module('Client', []);

client.controller('Cli', ['$scope','$http', function($scope, $http){    //linking our controller to our namespace ie institution

	$scope.clientForm = true;
	$scope.clientTable = false;

	$scope.createClient = function(){
		showHideFormTable(false, true);
	}

	$scope.showClient = function(){
		showHideFormTable(true, false);
	}

	function showHideFormTable(form, table){
		$scope.clientForm = form;
		$scope.clientTable = table;
	}

	$scope.client = {}; 
	$scope.clients = {};

	$scope.saveClient = function(){

		$http({
			url: '../api/client',
			method: 'POST',
			data: $.param($scope.client),
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).success(function(data){
			getClients();
			$scope.client = {};         

		});

	}

	function getClients(){
		$http.get('../api/client').
		success(function(data){
			$scope.clients = data.clients;

		});

	}

	getClients();

	$scope.loadEdit = function(idx){
		$http.get('../api/client/edit/' + $scope.clients[idx].id).
		success(function(data){           // $scope.ester
			$scope.client = data;
			showHideFormTable(false, true);

		});
	}

	$scope.delete = function(idx){
		$http({
			url: '../api/client/' + $scope.clients[idx].id,
			method: 'DELETE'
		}).success(function(data){
			getClients();

		});
	}

	$scope.search = function(){
		$http.get('../api/client/' + $scope.name_search).
		success(function(data){           
			$scope.clients = data.clients;

		});
	}

	$scope.reset = function(){
		getClients();
		$scope.name_search = '';
	}

}]);
