var fashion = angular.module('Fashion', []);

fashion.controller('Fash', ['$scope','$http', function($scope, $http){    //linking our controller to our namespace ie institution

	$scope.fashionForm = true;
	$scope.fashionTable = false;

	$scope.createFashion = function(){
		showHideFormTable(false, true);
	}

	$scope.showFashion = function(){
		showHideFormTable(true, false);
	}

	function showHideFormTable(form, table){
		$scope.fashionForm = form;
		$scope.fashionTable = table;
	}

	$scope.fashion = {}; 
	$scope.fashions = {};

	$scope.saveFashion = function(){

		$http({
			url: '../api/fashion',
			method: 'POST',
			data: $.param($scope.fashion),
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).success(function(data){
			getFashions();
			$scope.fashion = {};         

		});

	}

	function getFashions(){
		$http.get('../api/fashion').
		success(function(data){
			$scope.fashions = data.fashions;

		});

	}

	getFashions();

	$scope.loadEdit = function(idx){
		$http.get('../api/fashion/edit/' + $scope.fashions[idx].id).
		success(function(data){           // $scope.ester
			$scope.fashion = data;
			showHideFormTable(false, true);

		});
	}

	$scope.delete = function(idx){
		$http({
			url: '../api/fashion/' + $scope.fashions[idx].id,
			method: 'DELETE'
		}).success(function(data){
			getFashions();

		});
	}

	$scope.search = function(){
		$http.get('../api/fashion/' + $scope.name_search).
		success(function(data){           
			$scope.fashions = data.fashions;

		});
	}

	$scope.reset = function(){
		getFashions();
		$scope.name_search = '';
	}

}]);
